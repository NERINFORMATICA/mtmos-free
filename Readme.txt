/****************************************************************************
 *
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


MtmOs (Micro Task Manager Operating System)
Public release 1.04.06 - 03/2023

MtmOs is a free implementation of a very simple task scheduler in cooperative mode, where the task scheduler is running into the infinite main loop. 
All tasks are managed by a single call put into a main while(1) loop and are synchronous, atomic (excepted IRQ), deterministic.
To manage periodical task, one function must be called inside a periodic Timer ISR, in order to refresh all timers and set the task to execute.

Each task can be fired by periodical timers or by external events.
For deterministic state machine utility, MtmOs include an easy to use module which let to define state functions moved by events.

The MtmOs is easy to port on all uC based on cortexMx, but its very easy to move on other architecture defining specific OS_SAVE_EXCEPTION_MASK, OS_SAVE_EXCEPTION_MASK and DISABLE_IRQ macros

Download the last release here : https://gitlab.com/NERINFORMATICA/mtmos-free

The os_test module shall be used to check the functionality and performance of the scheduler, both with periodic timers (led blink) and with events (buttons).
The main port is working on a STM32WB55 Nucleo (3 buttons and 3 leds)

the module integration is very simple :

1. set the os_run() API into the main loop
2. set the os_refreshTimer() API into a timer ISR, to manage the periodic task
3. configure the system behaviour changing os_config.h
4. create a board directory under [ports] and add the include file into os_ports.h, defining the related macro (i.e. USE_NUCLEO_EVB)
5. to run test functions you have to define 3 gpio in input (buttons), 3 gpio in output (leds) and 5 gpio (DBG_GPIO_01_Pin) to check the performances using a logic state analyzer
6. set the test_init() API before the main infinite loop

BUTTONS: SET, UP, DOWN
LEDS: blink and setup

starting fw the led2 is blinking, led3 is off
pushing SET button the led3 start blinking fast: after few seconds led3 will be stable on. From now UP and DOWN button change the blinking periode of led2.
when no buttons are pushed and a timeout expire, led3 switch off and led2 blink using the last periode set.


run and adapt it ! 


Release notes

---------------------------------------------------------------------
1.04.06 - WIP
---------------------------------------------------------------------
- added new SM API to manage an internal timer for timed events
- added new SM API to add state/event for a sequence of function
- added note to os_delay API to DON'T USE IT inside task or state !! ;-)

---------------------------------------------------------------------
1.04.05
---------------------------------------------------------------------
- added os_delay API to delay execution
- Set os_IDLE task execution reason
- change if sequence in [statem_setStartState] API to prevent possible array out of index
- added FIFO module

---------------------------------------------------------------------
1.04.04
---------------------------------------------------------------------
- idle tasks added as standard task. 
- Added API os_setTask2RunInIdle to set task to run always in idle time
- removed OS_TASK_CFG_DO_IDLE_FN from os_config.h
---------------------------------------------------------------------
1.04.03
---------------------------------------------------------------------
- idle task executed with IRQ enabled
---------------------------------------------------------------------
1.04.02
---------------------------------------------------------------------
- changed event queue from lifo to fifo
---------------------------------------------------------------------
1.04.01
---------------------------------------------------------------------
- few patches
---------------------------------------------------------------------
1.04
---------------------------------------------------------------------
- added os_setMaxTaskExecutionTicks to set the execution time in ticks of the running task 
---------------------------------------------------------------------
1.03
---------------------------------------------------------------------
- added statistic tick delay and task tick execution report
- added os_getCurrentExecutionTicks() to get the execution time in ticks of the running task 
- added os_getCurrentTidRunning() to get the current tid inside a task running 
---------------------------------------------------------------------
1.02
---------------------------------------------------------------------
- added debug tick delay report
- fixed small bug when no task added
- added os_init function
- changed port directory and includes
- added stm32f446r2 nucleo port
- added #pragma pack() to reduce structure memory allocation
---------------------------------------------------------------------
1.01
---------------------------------------------------------------------
- optimized scheduler to reduce latency and skip unset task
- changed <> os include to ""
- moved test task indexes from 0
- performances on 48MHz cortex-M (release, -Ofast, including debug gpio API) :
	- ISR API function refresh : 3.14uS
	- Main run execution time : 3.56uS
	- latency between end ISR and start priority task : 3.68uS
---------------------------------------------------------------------
1.00
---------------------------------------------------------------------
- First release
