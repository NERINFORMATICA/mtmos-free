/****************************************************************************
 *
 *	 MtmOs : statem.c
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#include <stdio.h>
#include <cmsis_os.h>
#include "statem.h"

#define INDEX_EVENT			0
#define INDEX_NEXT_STATUS	1


static void thStateMachine(void const *argument);
static statemState_t intialStateIdx;


void statem_startThreads(statemVector_t *sm)
{
	// create task
	osThreadDef(SM, thStateMachine, osPriorityNormal, 0, 1024 );
	sm->myThreadHandle = osThreadCreate(osThread(SM), sm);
}

void statem_init(statemVector_t *sm)
{
	osMessageQDef_t queue;

	sm->myThreadHandle = NULL;
	// create queue
	queue.queue_sz = 10;
	queue.item_sz = sizeof(uint32_t);
	sm->queue = osMessageCreate(&queue, 0);

	statemState_t i;
	sm->mutex = osMutexCreate(NULL);
	sm->enabled = 0;
	sm->currStatusIndex = STATEM_MAX_STATES;

	for(i=0;i<STATEM_MAX_STATES;i++)
	{
		sm->states[i].stateCode = NULL;
		sm->states[i].eventsCount = 0;
	}

}

void statem_fireEvent(statemVector_t *sm, statemEvent_t event)
{
	// NOTE: can be called from ISR ..
	//osMutexWait(sm->mutex, osWaitForever);
	osMessagePut(sm->queue, event, osWaitForever);
	//osMutexRelease(sm->mutex);
}

uint32_t getNextStatusIndex(statemVector_t * sm, statemEvent_t event)
{
	uint32_t ret = -1;
	statemEvent_t loop;
	for(loop=0;loop<sm->states[sm->currStatusIndex].eventsCount;loop++)
	{
		if(sm->states[sm->currStatusIndex].events[loop][INDEX_EVENT] == event)
		{
			ret = sm->states[sm->currStatusIndex].events[loop][INDEX_NEXT_STATUS];
			break;
		}
	}
	return ret;
}

int32_t stateTransition(statemVector_t * sm, statemEvent_t event)
{
	int32_t stateIdx;
	osMutexWait(sm->mutex, osWaitForever);

	stateIdx = getNextStatusIndex(sm, event);
	if(stateIdx >= 0 && stateIdx < STATEM_MAX_STATES)
	{
		// refresh current status index
		sm->currStatusIndex = stateIdx;
		// execute current state function
		if(sm->states[stateIdx].stateCode)
		{
			osMutexRelease(sm->mutex);
			sm->states[stateIdx].stateCode(sm);
			osMutexWait(sm->mutex, osWaitForever);
		}
		// check if this state jump directly to the next
	}
	osMutexRelease(sm->mutex);
	return stateIdx;
}

static void thStateMachine(void const *argument)
{
  statemVector_t *sm = (statemVector_t *) argument;
  osEvent evt;
  for (;;)
  {
	  evt = osMessageGet(sm->queue, osWaitForever);
	  if(evt.status == osEventMessage && sm->enabled)
	  {
		  stateTransition(sm, evt.value.v);
	  }
	  while(stateTransition(sm, STATEM_EVENT_SKIP_TO_NEXT) >= 0);
  }
}

void statem_enable(statemVector_t * sm, uint8_t enable)
{
	osMutexWait(sm->mutex, osWaitForever);
	sm->enabled = enable;
	if(enable)
		statem_fireEvent(sm, STATEM_EVENT_START_SM);
	else
		sm->currStatusIndex = intialStateIdx;
	osMutexRelease(sm->mutex);
}

void statem_setStartState(statemVector_t * sm, statemState_t stateIdx)
{
	osMutexWait(sm->mutex, osWaitForever);
	if(stateIdx < STATEM_MAX_STATES && sm->states[stateIdx].stateCode != NULL)
	{
		intialStateIdx = stateIdx;
		// refresh current status index
		sm->currStatusIndex = stateIdx;
		osMutexRelease(sm->mutex);
		sm->states[stateIdx].stateCode(sm);
		osMutexWait(sm->mutex, osWaitForever);
	}
	osMutexRelease(sm->mutex);
}


void statem_addState(statemVector_t * sm, statemState_t stateIndex, statemFn stateFunction)
{
	if(!sm->enabled)
	{
		if(stateIndex < STATEM_MAX_STATES && sm->states[stateIndex].stateCode == NULL)
		{
			sm->states[stateIndex].stateCode = stateFunction;
		}
		else
		{
			// double set or index out of range !!! check it
			  critical_failure(__FILE__, __LINE__);
		}
	}
}

void statem_addTransition(statemVector_t * sm, statemState_t fromState,  statemEvent_t event, statemState_t nextState)
{
	if(!sm->enabled)
	{
		if(fromState < STATEM_MAX_STATES && sm->states[fromState].stateCode != NULL)
		{
			sm->states[fromState].events[sm->states[fromState].eventsCount][INDEX_EVENT] = event;
			sm->states[fromState].events[sm->states[fromState].eventsCount][INDEX_NEXT_STATUS] = nextState;
			sm->states[fromState].eventsCount++;
		}
		else
		{
			// state not set or index out of range !!! check it
			  critical_failure(__FILE__, __LINE__);
		}
	}
}
#if 0 // TODO
static void thStateMachineTimer(os_task_param_t *argument){
	statemVector_t *sm = (statemVector_t *)argument->specialDataStructure;
	if(argument->reason == os_TIMER){
		// fire event
		statem_fireEvent(sm, sm->myTimerTaskEvent);
		// switch off my timer
		os_setTaskPeriode(os_getCurrentTidRunning(), 0);
	}
}
#endif
// not implemented yet !
void statem_setInternalTimer(statemVector_t * sm, uint32_t ticks, uint32_t event){
#if 0
	sm->myTimerTaskEvent = event;
	os_setTaskPeriode(sm->myTimerTaskId, ticks);
#endif
}

void statem_insertListOfSequentialTask(statemVector_t * sm, const statemSequenceFn_t list[]){
	statemState_t stateIndex = 0;
	while(list[stateIndex].function != NULL){
		statem_addState(sm, stateIndex, list[stateIndex].function);
		statem_addTransition(sm, stateIndex, list[stateIndex].eventToJumpNextFn, stateIndex+1);
		stateIndex++;
	}
}
