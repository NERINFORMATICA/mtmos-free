/****************************************************************************
 *
 *	 MtmOs : statem.h
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef __STATEM__
#define __STATEM__

#include <cmsis_os.h>
#include <stdint.h>

typedef void (*statemFn) (void *args);

#define STATEM_MAX_STATES			32
#define STATEM_MAX_EVENTS			16

#define STATEM_EVENT_SKIP_TO_NEXT	0xffff
#define STATEM_EVENT_START_SM		0xfffe
#define STATEM_EVENT_NONE			0xfffd


typedef uint32_t 	statemEvent_t;
typedef uint16_t 	statemState_t;

typedef struct
{
	statemFn		stateCode;
	statemEvent_t	eventsCount;
	statemEvent_t	events[STATEM_MAX_EVENTS][2]; // 0=from; 1=to
}statemIitemLinked_t;


typedef struct
{
	osMutexId 			mutex;
	osMessageQId		queue;
	uint8_t				enabled;
	statemState_t		currStatusIndex;
	osThreadId			myThreadHandle;
	osThreadId			myTimerThreadHandle;
	uint32_t			myTimerTaskEvent;
	statemIitemLinked_t	states[STATEM_MAX_STATES];
}statemVector_t;

typedef struct{
	statemFn 		function;
	statemEvent_t	eventToJumpNextFn;
}statemSequenceFn_t;
/**
 * @brief init the state macnine structure.
 *
 * @param sm	pointer to the state machine structure
 *
 * @retval None
 */
void statem_init(statemVector_t * sm);

/**
 * @brief start the thread linked to the state machine
 *
 * @param sm	pointer to the state machine structure
 *
 * @retval None
 */
void statem_startThreads(statemVector_t *sm);

/**
 * @brief restart the state machine (jump to the start state)
 *
 * @param sm	pointer to the state machine structure
 *
 * @retval None
 */
void statem_restart(statemVector_t * sm);

/**
 * @brief enable the state machine
 *
 * @param sm	pointer to the state machine structure
 *
 * @retval None
 */
void statem_enable(statemVector_t * sm, uint8_t enable);

/**
 * @brief fire an event to the state machine
 *
 * @param sm		pointer to the state machine structure
 * @param enable	0=disabled, 1=enabled
 *
 * @retval None
 */
void statem_fireEvent(statemVector_t *sm, statemEvent_t event);

/**
 * @brief add a state to the state machine
 *
 * @param sm			pointer to the state machine structure
 * @param stateCode 	unique code of the state
 * @param stateFunction	callback to the state enter function
 *
 * @retval None
 */
void statem_addState(statemVector_t * sm, statemState_t stateCode, statemFn stateFunction);

/**
 * @brief add a transition between two states, linked to an event
 *
 * @param sm			pointer to the state machine structure
 * @param fromState 	unique code of the state which receive the event
 * @param event			unique code of the event
 * @param nextState		unique code of the destination state
 *
 * @retval None
 */
void statem_addTransition(statemVector_t * sm, statemState_t fromState,  statemEvent_t event, statemState_t nextState);

/**
 * @brief set the start state
 *
 * @param sm			pointer to the state machine structure
 * @param stateIndex 	unique code of the start state
 *
 * @retval None
 */
void statem_setStartState(statemVector_t * sm, statemState_t stateIndex);

/**
 * @brief set the internal timer for timed event 
 *
 * @param  sm: pointer to the state machine structure
 * @param  ticks: time in tick set into the timer. 0=OFF
 * @param  event: event to fire when time exipre
 * @retval None
 */
void statem_setInternalTimer(statemVector_t * sm, uint32_t ms, uint32_t event);

/**
 * @brief create a single list of sequential task. Use it instead statem_addState/statem_addTransition
 *
 * @param  sm: pointer to the state machine structure
 * @param  list: list of state and events to jump next fn. NULL terminate !
 * @retval None
 */
void statem_insertListOfSequentialTask(statemVector_t * sm, const statemSequenceFn_t list[]);


#endif
