/****************************************************************************
 *
 *	 MtmOs : fifo.c
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <stdlib.h>
#include <semaphore.h>
#include <malloc.h>

#include "fifo.h"
#include "memory.h"


struct s_fifo *fifo_create(int max_count)
{
	struct s_fifo *p_fifo_head = malloc_check(sizeof (struct s_fifo));
    pthread_mutex_init (&p_fifo_head->mutex,NULL );
    sem_init(&p_fifo_head->semaphore,0,0);
    p_fifo_head->p_fifo_last_element=NULL;
    p_fifo_head->p_fifo_next=NULL;
    p_fifo_head->max_count = max_count;
    p_fifo_head->count=0;
    return p_fifo_head;
}

int fifo_is_full(struct s_fifo *p_fifo_head){
	return fifo_count(p_fifo_head) >= p_fifo_head->max_count;
}

void fifo_push(struct s_fifo *p_fifo_head, void *data, int len)
{
	if (p_fifo_head != NULL)
	{
		if (fifo_count(p_fifo_head) < p_fifo_head->max_count)
		{
			t_fifo_node *fifo_node = malloc_check(sizeof (t_fifo_node));
			if (fifo_node != NULL)
			{
				pthread_mutex_lock(&p_fifo_head->mutex);
				fifo_node->element = malloc_check(len);
				memcpy(fifo_node->element, data, len);
				fifo_node->element_len = len;
				fifo_node->next_element = NULL;

				if (p_fifo_head->p_fifo_last_element == NULL)
				{
					p_fifo_head->p_fifo_next = p_fifo_head->p_fifo_last_element = fifo_node;
				}
				else
				{
					p_fifo_head->p_fifo_last_element->next_element = fifo_node;
					p_fifo_head->p_fifo_last_element = fifo_node;
				}

				p_fifo_head->count++;
				pthread_mutex_unlock(&p_fifo_head->mutex);
				sem_post(&p_fifo_head->semaphore);
			}
		}else{
			// TODO : ??
		}

	}
}

void *fifo_pop(struct s_fifo *p_fifo_head, int *len)
{
	sem_wait(&p_fifo_head->semaphore);

	pthread_mutex_lock(&p_fifo_head->mutex);

	t_fifo_node *temp_node;
	void *ret_val;

	if ((temp_node = p_fifo_head->p_fifo_next) == NULL)
	{
		if (len != NULL)
			*len = 0;
		return (NULL);
	}
	ret_val = temp_node->element;
	if (len != NULL)
	{
		*len = temp_node->element_len;
	}
	if ((p_fifo_head->p_fifo_next = temp_node->next_element) == NULL)
	{
		p_fifo_head->p_fifo_last_element = NULL;
	}
	free_check(temp_node);
	p_fifo_head->count--;

	pthread_mutex_unlock(&p_fifo_head->mutex);

	return (ret_val);
}

int fifo_count(struct s_fifo *p_fifo_head)
{
	pthread_mutex_lock(&p_fifo_head->mutex);
	int count = p_fifo_head->count;
	pthread_mutex_unlock(&p_fifo_head->mutex);
	return count;
}

int fifo_is_empty(struct s_fifo *p_fifo_head)
{
	pthread_mutex_lock(&p_fifo_head->mutex);
	int ret = p_fifo_head->p_fifo_next == NULL;
	pthread_mutex_unlock(&p_fifo_head->mutex);

	return ret;
}

void fifo_clear(struct s_fifo *p_fifo_head)
{
	char *p;
	int len;
	while(!fifo_is_empty(p_fifo_head))
	{
		p=fifo_pop(p_fifo_head, &len);
		if (p != NULL)
		{
			free_check(p);
		}
	}
}

void fifo_move(struct s_fifo *fifo_out, struct s_fifo *fifo_in)
{
	char *p;
	int len;
	while(!fifo_is_empty(fifo_in))
	{
		p=fifo_pop(fifo_in, &len);
		if (p != NULL)
		{
			fifo_push(fifo_out, p, len);
		}
	}
}

void fifo_append(struct s_fifo *fifo_out, struct s_fifo *fifo_in)
{
	char *p, *pc;
	int len;
	int count = fifo_count(fifo_in);

	while(count-- > 0)
	{
		p=fifo_pop(fifo_in, &len);
		if (p != NULL && len > 0)
		{
			pc = malloc_check(len);
			if(pc != NULL)
			{
				memcpy(pc, p, len);
				fifo_push(fifo_out, pc, len);
				fifo_push(fifo_in, p, len);
			}
			else
			{
				// TODO : ??
			}
		}
		else
		{
			fifo_push(fifo_out, p, len);
			fifo_push(fifo_in, p, len);
		}
	}
}

int fifo_contain(struct s_fifo *p_fifo_head, void *data, int len)
{
	int ret = 0;
	struct _fifo_node *p_node;
	pthread_mutex_lock(&p_fifo_head->mutex);

	p_node = p_fifo_head->p_fifo_next;
	while(p_node != NULL)
	{
		if (p_node->element_len == len && memcmp(data, p_node->element, len) == 0)
		{
			// found  !!
			ret = 1;
			break;
		}
		p_node = p_node->next_element;

	}
	pthread_mutex_unlock(&p_fifo_head->mutex);
	return ret;
}

