//****************************************************************************
 *
 *	 MtmOs : fifo.h
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef FIFO_H_
#define FIFO_H_

#include <semaphore.h>
#include <pthread.h>


#ifdef __cplusplus
extern "C"{
#endif


typedef struct _fifo_node
{
	void *element;
	int element_len;
	struct _fifo_node *next_element;
} t_fifo_node;

struct s_fifo
{
	t_fifo_node *p_fifo_next;
	t_fifo_node *p_fifo_last_element;
	pthread_mutex_t mutex;		// concurrent access
	sem_t semaphore;			// wakeup when push
	int max_count;				// max fifo elements
	int count;					// current elements
};

/**
* @brief Create a fifo.
*
* @param[in] max_count max number of fifo's element
*
* @retval pointer to fifo
*
*/
struct s_fifo *fifo_create(int max_count);

/**
 * @brief Push element on the fifo
 *
 * @param[in] p_fifo_head pointer to fifo
 * @param[in] data element to add
 */
void fifo_push(struct s_fifo *p_fifo_head, void *data, int len);

/**
 * @brief Remove an element from fifo
 *
 * @param[in] p_fifo_head pointer to fifo
 * @param[in] len pointer to len of element gets from fifo
 *
 *
 * @retval element removed
 */
void *fifo_pop(struct s_fifo *p_fifo_head, int *len);

/**
 * @brief Number of element on the fifo.
 *
 * @param[in] p_fifo_head pointer to fifo
 *
 * @retval number of element stored on the fifo.
 */
int fifo_count(struct s_fifo *p_fifo_head);

/**
 * @brief Check if the fifo is empty.
 *
 * @param[in] p_fifo_head pointer to fifo
 *
 * @retval true if fifo is empty
 */
int fifo_is_empty(struct s_fifo *p_fifo_head);

void fifo_clear(struct s_fifo *p_fifo_head);

void fifo_move(struct s_fifo *fifo_out, struct s_fifo *fifo_in);

void fifo_append(struct s_fifo *fifo_out, struct s_fifo *fifo_in);

int fifo_contain(struct s_fifo *p_fifo_head, void *data, int len);

int fifo_is_full(struct s_fifo *p_fifo_head);

#ifdef __cplusplus
}
#endif



#endif /* FIFO_H_ */
