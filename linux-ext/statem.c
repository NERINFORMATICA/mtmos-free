/****************************************************************************
 *
 *	 MtmOs : statem.c
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#include "statem.h"
#include "debug.h"
#include "fifo.h"

#define INDEX_EVENT			0
#define INDEX_NEXT_STATUS	1

void *thStateMachine(void *sm);
static statemState_t intialStateIdx;


void statem_startThreads(statemVector_t *sm)
{
	// create task
	if (pthread_create(&sm->myTaskId, NULL, thStateMachine, sm) != 0) {
		exit(1);
	}
}

void statem_init(statemVector_t *sm, char *name)
{
	// creare una coda di fifo
	sm->fifo = fifo_create(10);
	sm->enabled = FALSE;
	statemState_t i;
	sm->enabled = 0;
    	pthread_mutex_init(&sm->mutex, NULL);
	sm->smNameStr = name;
	sm->currStatusIndex = STATEM_MAX_STATES;
	sm->printDebugInfo = 0;
	for(i=0;i<STATEM_MAX_STATES;i++)
	{
		sm->states[i].stateCode = NULL;
		sm->states[i].eventsCount = 0;
	}
}

void statem_fireEvent(statemVector_t *sm, statemEvent_t event_bm)
{
	if(sm->enabled){
		if(sm->printDebugInfo){
			printf("%s - %s - event: %d\n", __FUNCTION__, sm->smNameStr, event_bm);
		}
		fifo_push(sm->fifo, &event_bm, sizeof(statemEvent_t));
	}
}

uint32_t getNextStatusIndex(statemVector_t * sm, statemEvent_t event_bm)
{
	uint32_t ret = -1;
	statemEvent_t loop;
	for(loop=0;loop<sm->states[sm->currStatusIndex].eventsCount;loop++)
	{
		if(sm->states[sm->currStatusIndex].events[loop][INDEX_EVENT] == event_bm)
		{
			ret = sm->states[sm->currStatusIndex].events[loop][INDEX_NEXT_STATUS];
			break;
		}
	}
	return ret;
}

int32_t stateTransition(statemVector_t * sm, statemEvent_t event_bm)
{
	int32_t stateIdx;
	if(!sm->enabled){
		return sm->currStatusIndex;
	}
	pthread_mutex_lock(&sm->mutex);

	stateIdx = getNextStatusIndex(sm, event_bm);
	if(stateIdx >= 0 && stateIdx < STATEM_MAX_STATES)
	{
		// refresh current status index
		sm->currStatusIndex = stateIdx;
		// execute current state function
		if(sm->states[stateIdx].stateCode){
			pthread_mutex_unlock(&sm->mutex);
			sm->states[stateIdx].stateCode(sm);
			if(sm->printDebugInfo){
				printf("stateTransition %s state %d executed\n", sm->smNameStr, stateIdx);
			}
			pthread_mutex_lock(&sm->mutex);
		}
		// check if this state jump directly to the next

	}else{
		// TODO: ??
	}
	pthread_mutex_unlock(&sm->mutex);
	return stateIdx;
}

void *thStateMachine(void *smInst) {
	statemVector_t *sm = (statemVector_t *) smInst;
	statemEvent_t *event_bm;
	while(1){
		event_bm = fifo_pop(sm->fifo, NULL);
		if(sm->enabled){
			if(event_bm){
				stateTransition(sm, *event_bm);
			}
			while(stateTransition(sm, STATEM_EVENT_SKIP_TO_NEXT) >= 0 && sm->enabled);
		}
		free(event_bm);
	}
}

void statem_enable(statemVector_t * sm, uint8_t enable)
{
	pthread_mutex_lock(&sm->mutex);
	sm->enabled = enable;
	if(enable){
		statem_fireEvent(sm, STATEM_EVENT_START_SM);
	}else{
		sm->currStatusIndex = intialStateIdx;
	}
	pthread_mutex_unlock(&sm->mutex);
}

void statem_setStartState(statemVector_t * sm, statemState_t stateIdx)
{
	pthread_mutex_lock(&sm->mutex);
	if(stateIdx < STATEM_MAX_STATES && sm->states[stateIdx].stateCode != NULL)
	{
		intialStateIdx = stateIdx;
		// refresh current status index
		sm->currStatusIndex = stateIdx;
		pthread_mutex_unlock(&sm->mutex);
		sm->states[stateIdx].stateCode(sm);
		pthread_mutex_lock(&sm->mutex);
	}
	pthread_mutex_unlock(&sm->mutex);
}


void statem_addState(statemVector_t * sm, statemState_t stateIndex, statemFn stateFunction)
{
	if(!sm->enabled)
	{
		if(stateIndex < STATEM_MAX_STATES && sm->states[stateIndex].stateCode == NULL)
		{
			sm->states[stateIndex].stateCode = stateFunction;
		}
		else
		{
			// double set or index out of range !!! check it
		}
	}
}

void statem_addTransition(statemVector_t * sm, statemState_t fromState,  statemEvent_t event, statemState_t nextState)
{
	if(!sm->enabled)
	{
		if(fromState < STATEM_MAX_STATES && sm->states[fromState].stateCode != NULL)
		{
			sm->states[fromState].events[sm->states[fromState].eventsCount][INDEX_EVENT] = event;
			sm->states[fromState].events[sm->states[fromState].eventsCount][INDEX_NEXT_STATUS] = nextState;
			sm->states[fromState].eventsCount++;
		}
		else
		{
			// state not set or index out of range !!! check it
		}
	}
}

static void thStateMachineTimer(os_task_param_t *argument){
#if 0 // TODO: not implemented yet !
	statemVector_t *sm = (statemVector_t *)argument->specialDataStructure;
	if(argument->reason == os_TIMER){
		// fire event
		statem_fireEvent(sm, sm->myTimerTaskEvent);
		// switch off my timer
		os_setTaskPeriode(os_getCurrentTidRunning(), 0);
	}
#endif
}

void statem_setInternalTimer(statemVector_t * sm, uint32_t ms, uint32_t event){
#if 0 // TODO: not implemented yet !
	sm->myTimerTaskEvent = event;
	os_setTaskPeriode(sm->myTimerTaskId, ms);
#endif
}

void statem_insertListOfSequentialTask(statemVector_t * sm, const statemSequenceFn_t list[]){
	statemState_t stateIndex = 0;
	while(list[stateIndex].function != NULL){
		statem_addState(sm, stateIndex, list[stateIndex].function);
		statem_addTransition(sm, stateIndex, list[stateIndex].eventToJumpNextFn, stateIndex+1);
		stateIndex++;
	}
}

