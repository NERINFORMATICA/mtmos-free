/****************************************************************************
 *
 *	 MtmOs : statem.h
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef __STATEM__
#define __STATEM__

#include <stdint.h>
#include <pthread.h>

typedef void (*statemFn) (void *args);

#define STATEM_MAX_STATES			255
#define STATEM_MAX_EVENTS			16

#define STATEM_EVENT_SKIP_TO_NEXT	0xffffffff
#define STATEM_EVENT_START_SM		0xfffffffe

#ifndef TRUE
#define TRUE				1
#define FALSE				0
#endif

typedef uint32_t 	statemEvent_t;
typedef uint16_t 	statemState_t;

typedef struct
{
	statemFn		stateCode;
	statemEvent_t	eventsCount;
	statemEvent_t	events[STATEM_MAX_EVENTS][2]; // 0=from; 1=to
}statemIitemLinked_t;

typedef struct
{
	uint8_t				enabled;
	statemState_t		currStatusIndex;
	pthread_t			myTaskId;
	pthread_t			myTimerTaskId;
	pthread_t			myTimerTaskEvent;
	char				*smNameStr;
	struct s_fifo 		*fifo;
	pthread_mutex_t		mutex;
	statemIitemLinked_t	states[STATEM_MAX_STATES];
	char				printDebugInfo; // 1== enbled
}statemVector_t;

typedef struct{
	statemFn 		function;
	statemEvent_t	eventToJumpNextFn;
}statemSequenceFn_t;



/**
 * @brief initialize the state machine data structure. Must be called before all other statem_ API call
 *
 * @param  sm: pointer to the state machine structure
 * @retval None
 */
void statem_init(statemVector_t *sm, char *name);

/**
 * @brief start the thread or task used to manage the state machine
 *
 * @param  sm: pointer to the state machine structure
 * @retval None
 */
void statem_startThreads(statemVector_t *sm);

/**
 * @brief restart the state machine: come back to the start state
 *
 * @param  sm: pointer to the state machine structure
 * @retval None
 */
void statem_restart(statemVector_t * sm);

/**
 * @brief enable or disable the state machine
 *
 * @param  sm: pointer to the state machine structure
 * @param  enable: true = enabled, false = disabled
 * @retval None
 */
void statem_enable(statemVector_t * sm, uint8_t enable);

/**
 * @brief fire an event the the state machine. It may change state if defined
 *
 * @param  sm: pointer to the state machine structure
 * @param  event_bm: the event
 * @retval None
 */
void statem_fireEvent(statemVector_t *sm, statemEvent_t event_bm);

/**
 * @brief add a state to the state machine structure. must be set before start the state machine
 *
 * @param  sm: pointer to the state machine structure
 * @param  stateCode: code of the state (index from 0)
 * @param  stateFunction: pointer of the state callback function, executed if the related state is selected
 * @retval None
 */
void statem_addState(statemVector_t * sm, statemState_t stateCode, statemFn stateFunction);

/**
 * @brief add transition between two states. More events can be added to a state
 *
 * @param  sm: pointer to the state machine structure
 * @param  fromState: code of the state from (index from 0)
 * @param  event_bm: the event
 * @param  nextState: the code of the destination state if the fired event is the one
 * @retval None
 */
void statem_addTransition(statemVector_t * sm, statemState_t fromState,  statemEvent_t event_bm, statemState_t nextState);

/**
 * @brief set the start state of the state machine. This API must be set before start state machine
 *
 * @param  sm: pointer to the state machine structure
 * @param  stateIndex: the state used as start state
 * @retval None
 */
void statem_setStartState(statemVector_t * sm, statemState_t stateIndex);

/**
 * @brief set the internal timer for timed event 
 *
 * @param  sm: pointer to the state machine structure
 * @param  ticks: time in ms set into the timer. 0=OFF
 * @param  event: event to fire when time exipre
 * @retval None
 */
void statem_setInternalTimer(statemVector_t * sm, uint32_t ms, uint32_t event);

/**
 * @brief create a single list of sequential task. Use it instead statem_addState/statem_addTransition
 *
 * @param  sm: pointer to the state machine structure
 * @param  list: list of state and events to jump next fn. NULL terminate !
 * @retval None
 */
void statem_insertListOfSequentialTask(statemVector_t * sm, const statemSequenceFn_t list[]);

#endif
