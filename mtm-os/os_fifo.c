/****************************************************************************
 *
 *	 MtmOs : os_fifo.c
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include "os_task.h"
#include "os_fifo.h"

#include "app_conf.h"
#include "dbg_trace.h"

static t_fifo_node gFifoStaticMemBuffer[FIFO_STATIC_ELEMENT_FIXED_NUMBER];
static uint8_t gFifoStaticMemBufferUsed[FIFO_STATIC_ELEMENT_FIXED_NUMBER];

void fifo_init()
{
	//APP_DBG_MSG(".FIFO Init\n");

    uint16_t i;
    for(i=0;i<FIFO_STATIC_ELEMENT_FIXED_NUMBER;i++)
    {
        gFifoStaticMemBufferUsed[i] = 0;
    }
}



t_fifo_node *fifoMalloc(size_t size)
{
    uint16_t i;
    t_fifo_node *ret = NULL;

    for(i=0;i<FIFO_STATIC_ELEMENT_FIXED_NUMBER;i++)    
    {
        if(!gFifoStaticMemBufferUsed[i])
        {
            gFifoStaticMemBufferUsed[i]=1;            
            ret = &gFifoStaticMemBuffer[i];
            break;
        }
    }

    return ret;    
};

void fifoFree(t_fifo_node *p)
{
    uint16_t i;
    for(i=0;i<FIFO_STATIC_ELEMENT_FIXED_NUMBER;i++)    
    {
        if(&gFifoStaticMemBuffer[i] == p)
        {
            gFifoStaticMemBufferUsed[i]=0;
            //osSemaphoreRelease(gStaticMemoryMutex);
            return;
        }
    }

    // memory hadler not valid ??
    // while(1);
}

void fifo_create(struct s_fifo *p_fifo_head, uint8_t max_count, uint8_t max_record_len, uint32_t wakeupTaskId, uint32_t wakeupEventId)
{
	p_fifo_head->wakeupTaskId = wakeupTaskId;
	p_fifo_head->wakeupEventId = wakeupEventId;
    p_fifo_head->p_fifo_last_element=NULL;
    p_fifo_head->p_fifo_next=NULL;
    p_fifo_head->max_count = max_count;
    p_fifo_head->max_record_len = max_record_len;
    p_fifo_head->count=0;
}

void fifo_push(struct s_fifo *p_fifo_head, void *data, uint16_t len)
{
	if (p_fifo_head != NULL)
	{
		if (fifo_count(p_fifo_head) < p_fifo_head->max_count)
		{
			t_fifo_node *fifo_node = fifoMalloc(sizeof (t_fifo_node));
			if (fifo_node != NULL)
			{
                memcpy(fifo_node->element, data, len);
				fifo_node->element_len = len;
				fifo_node->next_element = NULL;

				if (p_fifo_head->p_fifo_last_element == NULL)
				{
					p_fifo_head->p_fifo_next = p_fifo_head->p_fifo_last_element = fifo_node;
				}
				else
				{
					p_fifo_head->p_fifo_last_element->next_element = fifo_node;
					p_fifo_head->p_fifo_last_element = fifo_node;
				}

				p_fifo_head->count++;
				os_wakeupTask(p_fifo_head->wakeupTaskId, p_fifo_head->wakeupEventId);
			}
		}
	}
}

void fifo_pop(struct s_fifo *p_fifo_head, uint8_t *data, uint16_t *len)
{
	t_fifo_node *temp_node;
	if ((temp_node = p_fifo_head->p_fifo_next) == NULL)
	{
		if (len != NULL)
        {
			*len = 0;
        }
        return;
	}
    if(data != NULL)
    {
        memcpy(data, temp_node->element, temp_node->element_len);
    }
	if (len != NULL)
	{
		*len = temp_node->element_len;
	}
	if ((p_fifo_head->p_fifo_next = temp_node->next_element) == NULL)
	{
		p_fifo_head->p_fifo_last_element = NULL;
	}
	fifoFree(temp_node);
	p_fifo_head->count--;
}

int fifo_count(struct s_fifo *p_fifo_head)
{
	int count = p_fifo_head->count;
	return count;
}

int fifo_isEmpty(struct s_fifo *p_fifo_head)
{
	int ret = p_fifo_head->p_fifo_next == NULL;

	return ret;
}

uint8_t fifo_isNear34Full(struct s_fifo *p_fifo_head)
{
    uint16_t count = 0;
	count = p_fifo_head->count;
    // already set to near to full, check the low histeresys level
    if(p_fifo_head->isNearToBeFull)
    {
        p_fifo_head->isNearToBeFull = count <= (p_fifo_head->max_count / 4);
    }
    else
    {
        p_fifo_head->isNearToBeFull = count >= ((p_fifo_head->max_count / 4) * 3);
    }
    
    return p_fifo_head->isNearToBeFull;
}

uint8_t fifo_isFull(struct s_fifo *p_fifo_head)
{
    uint8_t ret = 0;
	int count = p_fifo_head->count;
    ret = count >= (p_fifo_head->max_count - 1);
	return ret;
}

void fifo_clear(struct s_fifo *p_fifo_head)
{
    while(!fifo_isEmpty(p_fifo_head))
    {
        fifo_pop(p_fifo_head, NULL, NULL);
    }
}
