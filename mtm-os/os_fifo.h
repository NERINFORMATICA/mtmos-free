/****************************************************************************
 *
 *	 MtmOs : os_fifo.h
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef FIFO_H_
#define FIFO_H_

#include <stdint.h>


#ifdef __cplusplus
extern "C"{
#endif

#define FIFO_STATIC_ELEMENT_FIXED_DIMENSION 256
#define FIFO_STATIC_ELEMENT_FIXED_NUMBER    32
    
typedef struct _fifo_node
{
	uint8_t element[FIFO_STATIC_ELEMENT_FIXED_DIMENSION];
	uint16_t element_len;
	struct _fifo_node *next_element;
} t_fifo_node;

struct s_fifo
{
	t_fifo_node *p_fifo_next;
	t_fifo_node *p_fifo_last_element;
	uint32_t wakeupTaskId;		// task waked up when push
	uint32_t wakeupEventId;		// event sent on push
	uint16_t max_count;			// max fifo elements
	uint16_t max_record_len;	// max record len
	uint16_t count;				// current elements
    uint8_t isNearToBeFull;
};

/**
 * @brief init the fifo module Execute it before to run any other FIFO API.
 *
 * @param none
 * @retval None
 */
void fifo_init();

/**
 * @brief fill and initialize a FIFO Structure.
 *
 * @param p_fifo_head: the pointer at the fifo structure
 * @param max_count: max number of elements storable
 * @param max_record_len: max lenght of data storable into an item
 *
 * @retval None
 */
void fifo_create(struct s_fifo *p_fifo_head, uint8_t max_count, uint8_t max_record_len, uint32_t wakeupTaskId, uint32_t wakeupEventId);

/**
 * @brief push a value into the FIFO queue
 *
 * @param p_fifo_head: the pointer at the fifo structure
 * @param data: the pointer of the data to push. NOTE: the pointer is used as is without copying : do not pass a stack variable here !
 * @param len: lenght of the data
 *
 * @retval None
 */
void fifo_push(struct s_fifo *p_fifo_head, void *data, uint16_t len);
/**
 * @brief pop a data from FIFO waiting a specific timeout. the API wait until a new element is push on the FIFO queue or TO expire
 *
 * @param p_fifo_head: the pointer at the fifo structure
 * @param len: pointer of the lenght of the data
 * @param timeout: timeout in mS. if TO expire with no new elements in FIFO, length variable return 0.
 *
 * @retval pointer of the data structure gets from FIFO
 */
//void *fifo_popWaitingTo(struct s_fifo *p_fifo_head, uint16_t *len, uint32_t timeout);
/**
 * @brief pop a data from FIFO waiting forever. the API wait until a new element is push on the FIFO queue
 *
 * @param p_fifo_head: the pointer at the fifo structure
 * @param data:	pointer of the data structure gets from FIFO
 * @param len: pointer of the lenght of the data
 *
 * @retval none
 */
void fifo_pop(struct s_fifo *p_fifo_head, uint8_t *data, uint16_t *len);

/**
 * @brief clear all the FIFO elements
 *
 * @param p_fifo_head: the pointer at the fifo structure
 *
 * @retval None
 */
void fifo_clear(struct s_fifo *p_fifo_head);

/**
 * @brief return the fifo elemento count
 *
 * @param p_fifo_head: the pointer at the fifo structure
 *
 * @retval number of element stored
 */
int fifo_count(struct s_fifo *p_fifo_head);

/**
 * @brief return if the FIFO is empty
 *
 * @param p_fifo_head: the pointer at the fifo structure
 *
 * @retval 1 if is empty
 */
int fifo_isEmpty(struct s_fifo *p_fifo_head);

/**
 * @brief return if the FIFO is full for 3$ of the max items storable
 *
 * @param p_fifo_head: the pointer at the fifo structure
 *
 * @retval 1 if is > 3/4 full
 */
uint8_t fifo_isNear34Full(struct s_fifo *p_fifo_head);
/**
 * @brief return if the FIFO is FULL (no more space left)
 *
 * @param p_fifo_head: the pointer at the fifo structure
 *
 * @retval 1 if is full
 */
uint8_t fifo_isFull(struct s_fifo *p_fifo_head);

#ifdef __cplusplus
}
#endif



#endif /* FIFO_H_ */
