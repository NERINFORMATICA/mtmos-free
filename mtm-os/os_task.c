/****************************************************************************
 *
 *	 MtmOs : os_task.c
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/



#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "ports/os_ports.h"
#include "os_task.h"

OS_PRAGMA_PACK_1
typedef struct
{
	void (*TaskCb)( os_task_param_t *params );
	uint32_t statMaxDelayTickToCall;							// statistic: max nr. of ticks delay between timer expired and task is executed
	uint32_t statMaxTaskExecutionTick;							// statistic: max nr. of ticks expired during task execution
	uint32_t currentTick;										// current tick counter
	uint32_t periodicTick;										// 0 = disabled, > 0 = fired each ticks
	uint8_t taskEnabled;										// 0 = disabled
	uint32_t eventsQueue[MAX_EVENTS_QUEUE_COUNT];				// event queue
	int8_t eventsPushQueueIndex; 								// index of the current event pushed in queue. -1 if empty
	int8_t eventsPopQueueIndex; 								// index of the current event pop from queue. -1 if empty
	uint32_t eventsMask;										// active events mask
	uint8_t priority;											// priority
	uint8_t isIdle;												// is executed in idle time
	os_task_param_t params;										// task params
} scheduler_item_t;
OS_PRAGMA_PACK_0

// store all the task added
static scheduler_item_t task_list[OS_TASK_CFG_MAX_COUNT] = { 0 };
static int32_t schedulerTaskCount = -1;
static uint32_t statTick4CurrentTask = 0;
static int32_t current_task_running = -1;
static uint32_t cfgMaxTaskExecutionTicks = 10;
static volatile uint32_t tickWaitCounter = 0;

void os_init( void )
{
	memset(task_list, 0, sizeof(task_list));
	osPortInit();
}

void os_refreshTimer( void )
{
	static uint32_t taskIdx;
	static uint32_t dgbLostCounterTick;
#if OS_DEBUG_TIMING_ACTIVE
	os_portGpioWrite(DBG_GPIO_02_Port, DBG_GPIO_02_Pin, GPIO_PIN_SET);
#endif

	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	statTick4CurrentTask++;
    tickWaitCounter++;

	// check if ISR executed with no task added 
	for( taskIdx = 0; schedulerTaskCount >= 0 && taskIdx <= schedulerTaskCount; taskIdx++ )
	{
		// check if it is a periodic task and if it is enabled
		if(task_list[taskIdx].TaskCb &&
		   task_list[taskIdx].taskEnabled &&
		   task_list[taskIdx].periodicTick != OS_TASK_CFG_DISABLE_PERIODE)
		{
			// inc current tick
			task_list[taskIdx].currentTick++;
			// check if this task is delayed
			if(task_list[taskIdx].currentTick > task_list[taskIdx].periodicTick)
			{
				dgbLostCounterTick = task_list[taskIdx].currentTick - task_list[taskIdx].periodicTick;
				if(dgbLostCounterTick > task_list[taskIdx].statMaxDelayTickToCall)
				{
					task_list[taskIdx].statMaxDelayTickToCall = dgbLostCounterTick;
				}
			}
			// if time is done, enable task
			if(task_list[taskIdx].currentTick == task_list[taskIdx].periodicTick )
			{
				// set event
				if(task_list[taskIdx].eventsPushQueueIndex < MAX_EVENTS_QUEUE_COUNT)
				{
					task_list[taskIdx].params.reason |= os_TIMER;
					task_list[taskIdx].eventsPushQueueIndex++;
					task_list[taskIdx].eventsQueue[task_list[taskIdx].eventsPushQueueIndex] = 0x00;
				}
			}
		}
	}

	OS_RESTORE_EXCEPTION_MASK();

#if OS_DEBUG_TIMING_ACTIVE
	os_portGpioWrite(DBG_GPIO_02_Port, DBG_GPIO_02_Pin, GPIO_PIN_RESET);
#endif

}

// look for a task with highter priority to be started before this one
uint8_t lookForPriorityTask(uint32_t *tid, uint8_t priority)
{
	uint8_t ret = FALSE;
	uint32_t taskIdx;

	if(!tid || *tid > schedulerTaskCount)
		return ret;

	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	for( taskIdx = 0; taskIdx <= schedulerTaskCount && schedulerTaskCount >= 0; taskIdx++ )
	{
		// check if it is a task enabled with lower priority with events pending
		if(task_list[taskIdx].taskEnabled &&
		   task_list[taskIdx].priority < priority &&
		   task_list[taskIdx].eventsPushQueueIndex >= 0 )
		{
			// set current priority
			priority = task_list[taskIdx].priority;
			*tid = taskIdx;
			// found almost one
			ret = TRUE;
		}
	}
	OS_RESTORE_EXCEPTION_MASK();

	return ret;
}

void runNextIdleFunction()
{
	static int32_t current_idle_task_set = 0;
	if(current_idle_task_set >= OS_TASK_CFG_MAX_COUNT)
		current_idle_task_set = 0;
	if(task_list[current_idle_task_set].isIdle && task_list[current_idle_task_set].taskEnabled)
	{
		statTick4CurrentTask = 0;
		task_list[current_idle_task_set].params.reason |= os_IDLE;
		task_list[current_idle_task_set].TaskCb(&task_list[current_idle_task_set].params);
		if(statTick4CurrentTask > task_list[current_idle_task_set].statMaxTaskExecutionTick)
		{
			task_list[current_idle_task_set].statMaxTaskExecutionTick = statTick4CurrentTask;
		}
	}
	current_idle_task_set++;
}

void os_run( void )
{
	static uint32_t current_task_set = 0;
	static uint32_t nr_of_task_set = 0;
	static uint32_t task_idx;
	static uint8_t isIdle;
	static uint8_t isPriorityTaskFired;

#if OS_DEBUG_TIMING_ACTIVE
	os_portGpioWrite(DBG_GPIO_01_Port, DBG_GPIO_01_Pin, GPIO_PIN_SET);
#endif

	OS_SAVE_EXCEPTION_MASK();

	if(current_task_set > schedulerTaskCount)
	{
		isIdle = nr_of_task_set == 0;
		current_task_set = 0;
		nr_of_task_set = 0;
	}
	// set temporary task index
	task_idx = current_task_set;
	// if there is a priority task to execute, it take the place of the current task
	isPriorityTaskFired = lookForPriorityTask(&task_idx, task_list[task_idx].priority);

	if(task_idx >= OS_TASK_CFG_MAX_COUNT)
	{
		task_idx = 0;
		return;
	}

	DISABLE_IRQ();

	if(task_list[task_idx].taskEnabled && task_list[task_idx].params.reason != os_NONE)
	{
		// get current event from queue
		if( task_list[task_idx].eventsPushQueueIndex >= 0 )
		{
			task_list[task_idx].params.eventsMaskFired = task_list[task_idx].eventsQueue[task_list[task_idx].eventsPopQueueIndex];
			task_list[task_idx].eventsPopQueueIndex++;
		}
		statTick4CurrentTask = 0;
		current_task_running = task_idx;

		OS_RESTORE_EXCEPTION_MASK();

		// execute task with specific event mask
		task_list[task_idx].TaskCb(&task_list[task_idx].params);

		DISABLE_IRQ();

		current_task_running = -1;
		// update the current task execution time in ticks
		if(statTick4CurrentTask > task_list[task_idx].statMaxTaskExecutionTick)
		{
			task_list[task_idx].statMaxTaskExecutionTick = statTick4CurrentTask;
#if OS_TASK_BLOCK_SCHED_FOR_MAX_EXECUTION_TIME
			if(statTick4CurrentTask > cfgMaxTaskExecutionTicks)
			{
				// NOTE: block the scheduer to debug. Change it
				while(1);
			}
#endif
		}
		// reset counter
		task_list[task_idx].currentTick = 0;
		// reset task params if no more events are counted
		if(task_list[task_idx].eventsPushQueueIndex >= 0 && task_list[task_idx].eventsPopQueueIndex > task_list[task_idx].eventsPushQueueIndex)
		{
			task_list[task_idx].eventsPushQueueIndex = -1;
			task_list[task_idx].eventsPopQueueIndex = 0;
			task_list[task_idx].params.eventsMaskFired = 0;
			task_list[task_idx].params.reason = 0;
		}
		nr_of_task_set++;
	}

	OS_RESTORE_EXCEPTION_MASK();

	// if no task executed for highest priority
	if(!isPriorityTaskFired)
	{
		// set to next
		current_task_set++;
		if( current_task_set >= OS_TASK_CFG_MAX_COUNT )
		{
			current_task_set = 0;
		}
		// if no task are fired during a cycle, do idle
		if (isIdle)
		{
			runNextIdleFunction();
		}
	}
#if OS_DEBUG_TIMING_ACTIVE
	os_portGpioWrite(DBG_GPIO_01_Port, DBG_GPIO_01_Pin, GPIO_PIN_RESET);
#endif
	return;
}

/**
 *  this function can be nested
 */
void os_registerTask( uint32_t tid, uint8_t priority, void (*task)( os_task_param_t *params ), uint8_t enabled )
{
	// check parameters. If failed, exit
	if(tid >= OS_TASK_CFG_MAX_COUNT || !task || priority >= OS_TASK_CFG_MAX_PRIO_COUNT)
	{
		return;
	}

	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	if(tid > schedulerTaskCount || schedulerTaskCount == -1)
	{
		schedulerTaskCount = tid;
	}

	task_list[tid].TaskCb = task;
	task_list[tid].currentTick = 0;
	task_list[tid].priority = priority;
	task_list[tid].periodicTick = OS_TASK_CFG_DISABLE_PERIODE;
	task_list[tid].taskEnabled = enabled;
	task_list[tid].eventsPushQueueIndex = -1;
	task_list[tid].eventsPopQueueIndex = 0;
	task_list[tid].eventsMask = 0;
	task_list[tid].params.eventsMaskFired = 0;
	task_list[tid].params.reason = os_NONE;
	task_list[tid].params.specialDataStructure = NULL;

	OS_RESTORE_EXCEPTION_MASK();

	return;
}

void os_stopTask( uint32_t tid )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();
	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].currentTick = 0;
		task_list[tid].eventsPushQueueIndex = -1;
		task_list[tid].eventsPopQueueIndex = 0;
		task_list[tid].params.eventsMaskFired = 0;
		task_list[tid].params.reason = os_NONE;
		task_list[tid].taskEnabled = FALSE;
	}
	OS_RESTORE_EXCEPTION_MASK();
	return;
}

void os_startTask( uint32_t tid )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();
	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].taskEnabled = TRUE;
	}

	OS_RESTORE_EXCEPTION_MASK();

  return;
}

void os_wakeupTask( uint32_t tid, uint32_t events_mask_bm )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	if(tid < OS_TASK_CFG_MAX_COUNT && task_list[tid].taskEnabled && (task_list[tid].eventsMask & events_mask_bm))
	{
		if(task_list[tid].eventsPushQueueIndex < MAX_EVENTS_QUEUE_COUNT - 1)
		{
			task_list[tid].eventsPushQueueIndex++;
			task_list[tid].eventsQueue[task_list[tid].eventsPushQueueIndex] = events_mask_bm;
			task_list[tid].params.reason |= os_EVENT;
		}
#if 0
		else
		{
			PRINTF("TASK WARNING: too many events in queue. Skipped\n");
		}
#endif
	}
	OS_RESTORE_EXCEPTION_MASK();

  return;
}

int32_t os_getFirstFreeTid()
{
	int32_t tid=-1;
	int32_t taskIdx;

	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	for( taskIdx = 0; taskIdx < OS_TASK_CFG_MAX_COUNT; taskIdx++ )
	{
		if(!task_list[taskIdx].TaskCb)
		{
			tid = taskIdx;
			break;
		}
	}

	OS_RESTORE_EXCEPTION_MASK();

	return tid;
}


void os_setSpecialDataTask( uint32_t tid, void *params )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].params.specialDataStructure = params;
	}

	OS_RESTORE_EXCEPTION_MASK();
}


void os_setTaskPeriode( uint32_t tid, uint32_t periodicTick )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].periodicTick = periodicTick;
		task_list[tid].currentTick = 0;
	}

	OS_RESTORE_EXCEPTION_MASK();
}

void os_setTaskEventMask( uint32_t tid, uint32_t events_mask_bm )
{
	OS_SAVE_EXCEPTION_MASK();
	DISABLE_IRQ();

	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].eventsMask = events_mask_bm;
		task_list[tid].eventsPushQueueIndex = -1;
		task_list[tid].eventsPopQueueIndex = 0;
		task_list[tid].params.eventsMaskFired = 0;
		task_list[tid].params.reason = os_NONE;
	}

	OS_RESTORE_EXCEPTION_MASK();
}

int32_t os_getCurrentTidRunning()
{
	return current_task_running;
}

uint32_t os_getCurrentExecutionTicks()
{
	return statTick4CurrentTask;
}

void os_setMaxTaskExecutionTicks(uint32_t ticks)
{
	cfgMaxTaskExecutionTicks = ticks;
}

void os_setTask2RunInIdle( uint32_t tid, uint8_t runInIdle )
{
	if(tid < OS_TASK_CFG_MAX_COUNT)
	{
		task_list[tid].isIdle = runInIdle;
	}
}

void os_delay(uint32_t ticks)
{
	tickWaitCounter = 0;
	while(tickWaitCounter < ticks);
}

