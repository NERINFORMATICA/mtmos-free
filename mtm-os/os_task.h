/****************************************************************************
 *
 *	 MtmOs : os_task.h
 *   Copyright (C) 2014-2023 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef SRC_OS_TASK_H_
#define SRC_OS_TASK_H_


#include "os_task.h"

#include "ports/os_ports.h"

#define osWaitForever		0

typedef enum
{
// bit mapped
	os_NONE=0x00,
	os_TIMER=0x01,
	os_EVENT=0x02,
	os_IDLE=0x04,
}os_reason;

OS_PRAGMA_PACK_1
typedef struct
{
	os_reason reason;
	uint32_t eventsMaskFired;
	void *specialDataStructure;
} os_task_param_t;
OS_PRAGMA_PACK_0



/**
 * @brief This function must be called before use any os_ API to init variables
 *
 * @param  None
 * @retval None
 */
void os_init( void );


/**
 * @brief This function is called from timer IRQ to manage periodic task
 *
 * @param  None
 * @retval None
 */
void os_refreshTimer( void );

/**
 * @brief  refresh the task execution. Execute one task each call
 *
 * @param  None
 * @retval None
 */
void os_run( void );

/**
 * @brief This registers a task in the scheduler.
 *
 * @param tid: The task id
 * @param priority: priority of execution when event occour or timer is fired. 0 = max priority; OS_TASK_CFG_MAX_PRIO_COUNT = lowest priority
 * @param task: task function. Does not include infinite loops
 * @param enabled: initial state of the task
 *
 * @retval None
 */
void os_registerTask( uint32_t tid, uint8_t priority, void (*task)( os_task_param_t *params ), uint8_t enabled );

/**
 * @brief set special data structure to the task environment
 *
 * @param  tid: the task id
 * @param  params: data structure
 * @retval None
 */
void os_setSpecialDataTask( uint32_t tid, void *params );


/**
 * @brief wake up a task using an event mask
 *
 * @param  tid: the task id
 * @param  events_mask_bm: the event mask bit mapped
 * @retval None
 */
void os_wakeupTask( uint32_t tid, uint32_t events_mask_bm );


/**
 * @brief pause the task
 *
 * @param  tid: The task id
 * @retval None
 */
void os_stopTask( uint32_t tid );

/**
 * @brief resume the task
 * @param  tid: The task id
 * @retval None
 */
void os_startTask( uint32_t tid );


/**
 * @brief return the first task id free on the scheduler
 * @param  None
 * @retval the tid. -1 if no tid free
 */
int32_t os_getFirstFreeTid();


/**
 * @brief return the current task id
 * @param  None
 * @retval the tid
 */
int32_t os_getCurrentTidRunning();


/**
 * @brief return the current task execution delay in ticks
 * @param  None
 * @retval nr of ticks
 */
uint32_t os_getCurrentExecutionTicks();

/**
 * @brief set if the relted task must be executed in idle free time
 *
 * @param  None
 * @retval None
 */
void os_setTask2RunInIdle( uint32_t tid, uint8_t runInIdle );

/**
 * @brief set or change task periodicity
 * @param  tid: The task id
 * @param  periodicTick: nr. of tick
 * @retval None
 */
void os_setTaskPeriode( uint32_t tid, uint32_t periodicTick );

/**
 * @brief set or change task periodicity
 * @param  tid: The task id
 * @param  events_mask_bm: the event mask
 * @retval None
 */
void os_setTaskEventMask( uint32_t tid, uint32_t events_mask_bm );


/**
 * @brief set the max ticks used to execute a task, else can stop FW on while(1) to debug !
 * @param  ticks: nr. of tick
 * @retval None
 */
void os_setMaxTaskExecutionTicks(uint32_t ticks);

/**
 * @brief wait nr. of tick stopping execution flow. DON'T USE IT inside task or state function ! (is syncronous)
 * @param  ticks: nr. of tick
 * @retval None
 */
void os_delay(uint32_t ticks);

#endif /* SRC_OS_TASK_H_ */
