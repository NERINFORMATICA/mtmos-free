/****************************************************************************
 *
 *	 MtmOs : os_ports.h
 *   Copyright (C) 2014-2019 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef SRC_MTMOS_PORTS_OS_PORTS_H_
#define SRC_MTMOS_PORTS_OS_PORTS_H_

// example of simple port to stm32 nucleo board
#ifdef MTMOS_USE_NUCLEO_WB55_EVB
#include "ports/stm32/wb55-nucleo/stm32nucleo.h"
#include "ports/stm32/wb55-nucleo/os_config.h"
#endif

#ifdef USE_NUCLEO_F446RE_EVB
#include "ports/stm32/f446re/stm32nucleo.h"
#include "ports/stm32/f446re/os_config.h"
#endif

#ifdef USE_MKE18F512_EVB
#include "ports/nxp/mke18f512.h"
#include "ports/nxp/os_config.h"
#endif

#ifdef USE_SAMD21_XPRO_EVB
#include "./microchip/samd21_xpro.h"
#include "./microchip/os_config.h"
#endif

// include here other ports

#endif /* SRC_MTMOS_PORTS_OS_PORTS_H_ */
