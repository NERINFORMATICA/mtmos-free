/****************************************************************************
 *
 *	 MtmOs : os_config.h
 *   Copyright (C) 2014-2019 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef SRC_OS_OS_CONFIG_H_
#define SRC_OS_OS_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>

#define OS_DEBUG_TIMING_ACTIVE			0	// use gpio to show timing
#define OS_DEBUG_PRINT_ACTIVE			0	// print strings on UART

// max nr of task
#define	OS_TASK_CFG_MAX_COUNT			10

// max levels of priority
#define OS_TASK_CFG_MAX_PRIO_COUNT		5

// block the scheduler if one task is more that the max ticks set
#define OS_TASK_BLOCK_SCHED_FOR_MAX_EXECUTION_TIME		0

// max nr of events to store in queue .. more are lost
#define MAX_EVENTS_QUEUE_COUNT			2

#define OS_TASK_CFG_MAX_TIMER_PERIODE	1000	//uS
#define OS_TASK_CFG_DISABLE_PERIODE		0


#ifdef __cplusplus
}
#endif

#endif /* SRC_OS_OS_CONFIG_H_ */
