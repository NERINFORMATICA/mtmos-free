/****************************************************************************
 *
 *	 MtmOs : stm32nucleo.h
 *   Copyright (C) 2014-2019 Luciano Neri (NERINFORMATICA). All rights reserved.
 *   Author: Luciano Neri <l.neri@nerinformatica.it>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name MtmOs nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/


#ifndef SRC_MTMOS_PORTS_STM32_STM32NUCLEO_H_
#define SRC_MTMOS_PORTS_STM32_STM32NUCLEO_H_

#include <stm32wbxx_hal.h>


// simulating state machine events and commands
#if 0
#define B1_Pin 				GPIO_PIN_4
#define B1_GPIO_Port 		GPIOC
#define B2_Pin 				GPIO_PIN_0
#define B2_GPIO_Port 		GPIOD
#define B3_Pin 				GPIO_PIN_1
#define B3_GPIO_Port 		GPIOD

#define LD1_Pin 			GPIO_PIN_5
#define LD1_GPIO_Port 		GPIOB
#define LD2_Pin 			GPIO_PIN_0
#define LD2_GPIO_Port 		GPIOB
#define LD3_Pin 			GPIO_PIN_1
#define LD3_GPIO_Port 		GPIOB

// used to debug task timing
#define DBG_GPIO_01_Pin 	GPIO_PIN_0		// main loop time (on enter, off exit)
#define DBG_GPIO_01_Port 	GPIOC
#define DBG_GPIO_02_Pin 	GPIO_PIN_1		// timer ISR (on enter, off exit)
#define DBG_GPIO_02_Port 	GPIOC
#define DBG_GPIO_03_Pin 	GPIO_PIN_0		// periodic task (toggle every time)
#define DBG_GPIO_03_Port 	GPIOA
#define DBG_GPIO_04_Pin 	GPIO_PIN_3		// state machine event fired (toggle every time)
#define DBG_GPIO_04_Port 	GPIOC
#define DBG_GPIO_05_Pin 	GPIO_PIN_2		// state machine task changed (on enter, off exit)
#define DBG_GPIO_05_Port 	GPIOC
#endif

#define US_TO_TICK(a)			((a) / OS_TASK_CFG_MAX_TIMER_PERIODE)
#define MS_TO_TICK(a)			(((a) * 1000) / OS_TASK_CFG_MAX_TIMER_PERIODE)
#define SEC_TO_TICK(a)			(((a) * 1000 * 1000) / OS_TASK_CFG_MAX_TIMER_PERIODE)



#ifndef  NULL
#define NULL                    0U
#endif

#ifndef FALSE
#define FALSE                   0U
#endif

#ifndef  TRUE
#define TRUE                    (!0U)
#endif


/* -------------------------------- *
 *  Critical Section definition     *
 * -------------------------------- */

#undef OS_SAVE_EXCEPTION_MASK
#define OS_SAVE_EXCEPTION_MASK()    uint32_t primask_bit= __get_PRIMASK()

#undef DISABLE_IRQ
#define DISABLE_IRQ()       		__disable_irq()

#undef OS_RESTORE_EXCEPTION_MASK
#define OS_RESTORE_EXCEPTION_MASK()   __set_PRIMASK(primask_bit)

#undef OS_PRAGMA_PACK_0
#define OS_PRAGMA_PACK_0   _Pragma("pack(0)")

#undef OS_PRAGMA_PACK_1
#define OS_PRAGMA_PACK_1   _Pragma("pack(1)")


#define osPriorityNormal  ((OS_TASK_CFG_MAX_PRIO_COUNT - 1) / 2)
#define osPriorityHighest  0
#define osPrioritylowest   (OS_TASK_CFG_MAX_PRIO_COUNT - 1)



#define os_portGpioRead(a, b)		HAL_GPIO_ReadPin(a, b)
#define os_portGpioToggle(a,b)		HAL_GPIO_TogglePin(a, b)
#define os_portGpioWrite(a,b,c)		HAL_GPIO_WritePin(a,b,c)

void osPortUartDebugPrintString(char *string);
void osPortInit();


#endif /* SRC_MTMOS_PORTS_STM32_STM32NUCLEO_H_ */
